import { TajwalHotelsPage } from './app.po';

describe('tajwal-hotels App', () => {
  let page: TajwalHotelsPage;

  beforeEach(() => {
    page = new TajwalHotelsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
