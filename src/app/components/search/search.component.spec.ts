import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MdDatepickerModule, MdNativeDateModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from '../../app.routes';
import { SearchComponent } from './search.component';
import { ReservationDates } from '../../Models/dates.model';

describe('SearchComponent', () => {
    const startDate = new Date();
    const endDate = new Date();
    endDate.setDate(endDate.getDate() + 5);
    let component: SearchComponent;
    let fixture: ComponentFixture<SearchComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, MdDatepickerModule, MdNativeDateModule, RouterTestingModule.withRoutes([
                { path: 'search', component: SearchComponent }
            ])],
            declarations: [SearchComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SearchComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });

    it('should call hotels page with start and end time', () => {
        component.reservationDates = new ReservationDates(startDate, endDate);
        spyOn(component.router, 'navigate');
        component.onSubmit();
        expect(component.router.navigate).toHaveBeenCalledWith(['/hotels',
                component.reservationDates.startDate.getTime(), component.reservationDates.endDate.getTime()]);
    });
});
