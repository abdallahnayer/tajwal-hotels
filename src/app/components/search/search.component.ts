import { Component, OnInit, Input } from '@angular/core';
import { ReservationDates } from '../../Models/dates.model';
import { Router } from '@angular/router';

@Component({
  selector: 'taj-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Input()
  reservationDates: ReservationDates = new ReservationDates();

  constructor(public router: Router) {
   }

  minDate = new Date();

  ngOnInit() {
  }

  onSubmit() {
    this.router.navigate(['/hotels', this.reservationDates.startDate.getTime(), this.reservationDates.endDate.getTime()]);
  }

}
