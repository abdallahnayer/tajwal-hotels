import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ReservationDates } from '../../Models/dates.model';
import { Hotel, HotelsPrices } from '../../Models/hotels.model';
import { HotelsService } from '../../services/hotels.service';

@Component({
  selector: 'taj-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.scss']
})
export class HotelsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private hotelsService: HotelsService) { }

  reservationDates: ReservationDates;

  hotels: Hotel[];

  totalNights: number;

  hotelsPrices: HotelsPrices;

  ngOnInit() {
    this.route.params.subscribe(params => {
      const startDate = Number(params['startDate']);
      const endDate = Number(params['endDate']);
      this.totalNights = this.hotelsService.getTotalReservationNights(startDate, endDate);
      this.reservationDates = new ReservationDates(new Date(startDate), new Date(endDate));
      this.hotelsService.getHotels(this.reservationDates, this.totalNights).subscribe((hotels: Hotel[]) => {
        this.hotels = hotels;
        this.hotelsPrices = this.hotelsService.getMinMaxPrices();
        // For the slider steps
        this.hotelsPrices.min = Math.floor(this.hotelsPrices.min / 5) * 5;
        this.hotelsPrices.max = Math.ceil(this.hotelsPrices.max / 5) * 5;
      });
    });
  }

  sortHotelsList(propName, mode) {
    this.hotelsService.sortHotelsList(propName, mode);
  }

}
