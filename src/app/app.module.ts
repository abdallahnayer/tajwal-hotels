import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MdDatepickerModule, MdNativeDateModule, MdSliderModule } from '@angular/material';
import { ServicesModule } from './services/services.module';
import { RouterModule } from '@angular/router';

import { routes } from './app.routes';
import { AppComponent } from './app.component';
import { SearchComponent } from './components/search/search.component';
import { HotelsComponent } from './components/hotels/hotels.component';
import { SortHotelsDirective } from './directives/sort-hotels.directive';
import { FilterHotelsDirective } from './directives/filter-hotels.directive';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    HotelsComponent,
    SortHotelsDirective,
    FilterHotelsDirective
  ],
  imports: [
    BrowserModule,
    MdDatepickerModule,
    MdNativeDateModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
    ServicesModule,
    MdSliderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
