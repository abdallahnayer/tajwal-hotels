// Sort array of strings asc or desc
export function sortStringsArray(array, propertyName, ascMode) {
    if (ascMode) {
        array.sort(function (a, b) {
            return a[propertyName].localeCompare(b[propertyName]);
        });
    } else {
        array.sort(function (a, b) {
            return b[propertyName].localeCompare(a[propertyName]);
        });
    }
}
// Sort array of numbers asc or desc
export function sortNumbersArray(array, propertyName, ascMode) {
    if (ascMode) {
        array.sort(function (a, b) {
            return a[propertyName] - b[propertyName];
        });
    } else {
        array.sort(function (a, b) {
            return b[propertyName] - a[propertyName];
        });
    }
}
