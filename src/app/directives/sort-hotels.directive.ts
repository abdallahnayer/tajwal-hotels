import { Directive, OnInit, HostListener, Input, Renderer2, ElementRef } from '@angular/core';
import { HotelsService } from '../services/hotels.service';

/**
 * Dynamic directive used to apply sorting on the hotels, different hotel's property object must be supplied
 * to the directive to know exactly the soring will be based on which property
 */
@Directive({
  selector: '[tajSortHotels]'

})
export class SortHotelsDirective implements OnInit {

  constructor(private hotelsService: HotelsService, private renderer: Renderer2, private hostElement: ElementRef) {
    this.hotelsService.clearSort.subscribe((propertyName: string) => {
      if (propertyName !== this.sortProperty) {
        this.ascMode = undefined;
        this.renderer.removeClass(this.hostElement.nativeElement.children[0], 'glyphicon-arrow-up');
        this.renderer.removeClass(this.hostElement.nativeElement.children[0], 'glyphicon-arrow-down');
      } else if (propertyName === this.sortProperty) {
        if (this.ascMode) {
          this.renderer.removeClass(this.hostElement.nativeElement.children[0], 'glyphicon-arrow-up');
          this.renderer.addClass(this.hostElement.nativeElement.children[0], 'glyphicon-arrow-down');
        } else {
          this.renderer.removeClass(this.hostElement.nativeElement.children[0], 'glyphicon-arrow-down');
          this.renderer.addClass(this.hostElement.nativeElement.children[0], 'glyphicon-arrow-up');
        }
      }
    });
  }

  @Input()
  sortProperty: string;

  ascMode: boolean;

  ngOnInit() {

  }

  @HostListener('click')
  sortHotelsList() {
    this.hotelsService.clearSorting(this.sortProperty);
    this.ascMode = !this.ascMode;
    this.hotelsService.sortHotelsList(this.sortProperty, this.ascMode);
  }

}
