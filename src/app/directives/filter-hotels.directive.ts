import { Directive, OnInit, HostListener, Input, OnChanges } from '@angular/core';
import { HotelsService } from '../services/hotels.service';

/**
 * Dynamic filtering directive that can be applied to different html elements(input, select, checkbox, slider)
 * The filterType is the type of the html elemnent must be provided (inputtext, checbox, select, mdSlider)
 * to be used to get the filtervalue
 * The filter property of the hotel object that will be used for filteration must be provided
 * The filterValue must only be provided in case of checkbox
 */
@Directive({
  selector: '[tajFilterHotels]'

})
export class FilterHotelsDirective implements OnInit {

  constructor(private hotelsService: HotelsService) {
  }

  @Input()
  filterProperty: string;

  @Input()
  filterValue: string;

  @Input()
  filterType: string;

  private _filterValue: string;

  ngOnInit() {

  }

  @HostListener('change', ['$event'])
  filterHotelsList(event: any) {
    const filteredValue = this.getFilterValue(event);
    this.hotelsService.filterHotelsList(this.filterProperty, filteredValue);
  }

  private getFilterValue(event) {
    let filteredValue = '';
    switch (this.filterType) {
      case 'checkbox':
        filteredValue = event.target.checked ? this.filterValue : '';
        break;
      case 'mdSlider':
        filteredValue = event.value;
        break;
      case 'inputText':
        filteredValue = event.target.value;
        break;
      case 'select':
        filteredValue = event.target.value;
        break;
      default:
        break;

    }
    this._filterValue = filteredValue;
    return this._filterValue;
  }
}
