import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ReservationDates } from '../Models/dates.model';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { Hotels, Hotel, Availability, HotelsPrices } from '../Models/hotels.model';
import { sortStringsArray, sortNumbersArray } from '../util';

@Injectable()
export class HotelsService {

  constructor(private http: Http) { }

  clearSort: Subject<string> = new Subject<string>();

  hotels: Hotel[] = [];

  filters = {};

  /**
   *
   * @param dates The reservation dates for the stay
   * @param totalNights totalNights of the stay
   * This function filters the list of hotels according to the reservationdates and reutrns only
   * the available hotels
   */
  getHotels(dates: ReservationDates, totalNights: number): Observable<Hotel[]> {
    this.hotels = [];
    return this.http.get('https://api.myjson.com/bins/tl0bp').map((response) => {
      const hotels = (response.json() as Hotels).hotels;
      for (let i = 0; i < hotels.length; i++) {
        const currHotel = hotels[i];
        for (let j = 0; j < currHotel.availability.length; j++) {
          const currHotelAvai: Availability = currHotel.availability[j];
          if (dates.startDate >= new Date(currHotelAvai.from) && dates.endDate <= new Date(currHotelAvai.to)) {
            currHotel.reservationPrice = currHotel.price * totalNights;
            this.hotels.push(currHotel);
            break;
          }
        }
      }
      return this.hotels;
    });
  }

  /**
   *
   * @param startDate The start date of the reservation
   * @param endDate The end date of the reservation
   * This function retruns the total reservation days
   */
  getTotalReservationNights(startDate: number, endDate: number) {
    return Math.ceil((endDate - startDate) / (1000 * 60 * 60 * 24));
  }

 /**
  *
  * @param propertyName The propertyname used for the filteration
  * @param ascMode The sorting mode(ascending or descending)
  * This function sorts the hotel array dynamically with any property in the hotel object provided
  */
  sortHotelsList(propertyName: string, ascMode: boolean) {
    if (this.hotels.length === 0) {
      return;
    }
    if (typeof this.hotels[0][propertyName] === 'string') {
      sortStringsArray(this.hotels, propertyName, ascMode);
    } else if (typeof this.hotels[0][propertyName] === 'number') {
      sortNumbersArray(this.hotels, propertyName, ascMode);
    }
  }

  /**
   *
   * @param propertyName The current propertyname used for sorting
   * This function notifies all sorting listeners to clear the current sorting applied and apply the new sorting
   */
  clearSorting(propertyName: string) {
    this.clearSort.next(propertyName);
  }

  /**
   *
   * @param propertyName The property name used for filtering
   * @param filterValue The value used for filtering
   * This is a dynamic function used to apply multiple filtering conditions on the hotels array
   * using dynamic properties of the hotel object
   */
  filterHotelsList(propertyName: string, filterValue: string) {
    if (this.hotels.length === 0) {
      return;
    }
    this.filters[propertyName] = filterValue;

    this.hotels.forEach((hotel: Hotel) => {

      let hotelDisplay = true;

      for (const filterName in this.filters) {
        if (typeof hotel[filterName] === 'string') {
          if (hotel[filterName].toLowerCase().indexOf(this.filters[filterName].toLowerCase()) === -1) {
            hotelDisplay = false;
            break;
          }
        } else if (typeof hotel[filterName] === 'number') {
          const filterValueNum = Number(this.filters[filterName]);
          if (hotel[filterName] > filterValueNum) {
            hotelDisplay = false;
            break;
          }
        }
      }
      hotel.display = hotelDisplay;
    });
  }

  /**
   * This function gets the max and min prices for the whole reservation
   */
  getMinMaxPrices(): HotelsPrices {
    if (this.hotels.length === 0) {
      return { min: 0, max: 0 } as HotelsPrices;
    }
    return this.hotels.reduce(
      (hotelsMinMax: HotelsPrices, hotel: Hotel) => {
        hotelsMinMax.min = hotel.reservationPrice < hotelsMinMax.min ? hotel.reservationPrice : hotelsMinMax.min;
        hotelsMinMax.max = hotel.reservationPrice > hotelsMinMax.max ? hotel.reservationPrice : hotelsMinMax.max;
        return hotelsMinMax;
      }
      ,
      { min: this.hotels[0].reservationPrice, max: this.hotels[0].reservationPrice });
  }
}
