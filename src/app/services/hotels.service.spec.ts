import { TestBed, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import {
  HttpModule, Http,
  Response,
  ResponseOptions,
  XHRBackend
} from '@angular/http';

import { HotelsService } from './hotels.service';
const startDate = new Date();
const endDate = new Date();
endDate.setDate(endDate.getDate() + 5);
const mockResponse = {
    'hotels': [{
      'name': 'Media One Hotel',
      'price': 102.2,
      'city': 'dubai',
      'availability': [{
        'from': startDate,
        'to': endDate
      }, {
        'from': '25-10-2020',
        'to': '15-11-2020'
      }, {
        'from': '10-12-2020',
        'to': '15-12-2020'
      }]
    }, {
      'name': 'Rotana Hotel',
      'price': 80.6,
      'city': 'cairo',
      'availability': [{
        'from': '10-10-2020',
        'to': '12-10-2020'
      }, {
        'from': '25-10-2020',
        'to': '10-11-2020'
      }, {
        'from': '05-12-2020',
        'to': '18-12-2020'
      }]
    }, {
      'name': 'Le Meridien',
      'price': 89.6,
      'city': 'london',
      'availability': [{
        'from': startDate,
        'to': endDate
      }, {
        'from': '05-10-2020',
        'to': '10-11-2020'
      }, {
        'from': '05-12-2020',
        'to': '28-12-2020'
      }]
    }, {
      'name': 'Golden Tulip',
      'price': 109.6,
      'city': 'paris',
      'availability': [{
        'from': startDate,
        'to': endDate
      }, {
        'from': '16-10-2020',
        'to': '11-11-2020'
      }, {
        'from': '01-12-2020',
        'to': '09-12-2020'
      }]
    }, {
      'name': 'Novotel Hotel',
      'price': 111,
      'city': 'Vienna',
      'availability': [{
        'from': '20-10-2020',
        'to': '28-10-2020'
      }, {
        'from': '04-11-2020',
        'to': '20-11-2020'
      }, {
        'from': '08-12-2020',
        'to': '24-12-2020'
      }]
    }, {
      'name': 'Concorde Hotel',
      'price': 79.4,
      'city': 'Manila',
      'availability': [{
        'from': '10-10-2020',
        'to': '19-10-2020'
      }, {
        'from': '22-10-2020',
        'to': '22-11-2020'
      }, {
        'from': '03-12-2020',
        'to': '20-12-2020'
      }]
    }]
};

describe('HotelsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [HotelsService, { provide: XHRBackend, useClass: MockBackend }]
    });
  });

  it('should be created', inject([HotelsService], (service: HotelsService) => {
    expect(service).toBeTruthy();
  }));
  it('should filter hotels according to availability',
    inject([HotelsService, XHRBackend], (service: HotelsService, mockBackend: MockBackend) => {
      mockBackend.connections.subscribe((connection: any) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(mockResponse)
        })));
    });
    service.getHotels({startDate: startDate, endDate: endDate}, 5).subscribe((hotels) => {
        expect(hotels.length).toBe(3);
    });
  }));
   it('should add reservation price to the hotel respective to number of nights',
    inject([HotelsService, XHRBackend], (service: HotelsService, mockBackend: MockBackend) => {
      mockBackend.connections.subscribe((connection: any) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(mockResponse)
        })));
    });
    service.getHotels({startDate: startDate, endDate: endDate}, 5).subscribe((hotels) => {
        for (let i = 0; i < hotels.length; i++) {
          expect(hotels[i].reservationPrice).toBe(hotels[i].price * 5);
        }
    });
  }));
  it('should calculate the reservation nights',
    inject([HotelsService, XHRBackend], (service: HotelsService, mockBackend: MockBackend) => {
    const numberOfNights = service.getTotalReservationNights(startDate.getTime(), endDate.getTime());
    expect(numberOfNights).toBe(5);
  }));
  it('should sort hotels according to dynamic object properties',
    inject([HotelsService, XHRBackend], (service: HotelsService, mockBackend: MockBackend) => {
      service.hotels = <any>mockResponse.hotels;
      const hotelsSortedName =  [{'name': 'Concorde Hotel'}, {'name': 'Golden Tulip'},
       {'name': 'Le Meridien'}, {'name': 'Media One Hotel' },
      {'name': 'Novotel Hotel'}, {'name': 'Rotana Hotel'} ];
      const hotelsSortedPrice =  [{'price': 79.4}, {'price': 80.6},
      {'price': 89.6}, {'price': 102.2 },
     {'price': 109.6}, {'price': 111} ];
      service.sortHotelsList('name', true);
      for (let i = 0; i < service.hotels.length; i++) {
        expect(service.hotels[i].name).toBe(hotelsSortedName[i].name);
      }
      service.sortHotelsList('price', true);
      for (let i = 0; i < service.hotels.length; i++) {
        expect(service.hotels[i].price).toBe(hotelsSortedPrice[i].price);
      }
  }));
});
