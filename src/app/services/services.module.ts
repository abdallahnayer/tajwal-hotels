import { NgModule } from '@angular/core';
import { HotelsService } from './hotels.service';

@NgModule({
  declarations: [
  ],
  exports: [
  ],
  providers: [
    HotelsService
  ]
})
export class ServicesModule { }
