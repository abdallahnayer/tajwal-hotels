export interface Hotels {
    hotels: Hotel[];
}

export interface Hotel {
    name: string;
    price: number;
    city: string;
    availability: Availability[];
    display: boolean;
    reservationPrice: number;
}

export interface Availability {
    from: string;
    to: string;
}

export interface HotelsPrices {
    min: number;
    max: number;
}
