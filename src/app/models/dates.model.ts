export class ReservationDates {
    startDate: Date;
    endDate: Date;
    constructor(startDate?: Date, endDate?: Date) {
        if (!startDate) {
            startDate = new Date();
            endDate = new Date();
            endDate.setDate(endDate.getDate() + 1);
        }
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
