import { Routes } from '@angular/router';
import { SearchComponent } from './components/search/search.component';
import { HotelsComponent } from './components/hotels/hotels.component';

export const routes: Routes = [
    { path: 'hotels/:startDate/:endDate', component: HotelsComponent },
    { path: 'search', component: SearchComponent, pathMatch: 'full' },
    { path: '**', redirectTo: '/search', pathMatch: 'full' }
];
